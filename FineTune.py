import tensorflow as tf

class FineTune:
    def finetune(model, new_dataset):
        loss_fn = tf.keras.losses.BinaryCrossentropy(from_logits=True)
        optimizer = tf.keras.optimizers.Adam()

        # Iterate over the batches of a dataset.
        for inputs, targets in new_dataset:
            # Open a GradientTape.
            with tf.GradientTape() as tape:
                # Forward pass.
                predictions = model(inputs)
                # Compute the loss value for this batch.
                loss_value = loss_fn(targets, predictions)

            # Get gradients of loss wrt the *trainable* weights.
            gradients = tape.gradient(loss_value, model.trainable_weights)
            # Update the weights of the model.
            optimizer.apply_gradients(zip(gradients, model.trainable_weights))